package p01to09

import org.specs2.mutable.Specification
import scala.annotation.tailrec

class P05Spec extends Specification {
  import P05.reverse

  "Reverse a list" >> {
    reverse(List(1, 1, 2, 3, 5, 8)) must_== List(8, 5, 3, 2, 1, 1)
  }

  "Reversing an empty list gives an empty list" >> {
    reverse(List.empty[Int]) must_== List.empty[Int]
  }

  "Reversing a list with a single entry returns the same list" >> {
    reverse(List(1)) must_== List(1)
  }
}

object P05 {
  def reverse[A](list: List[A]): List[A] = {
    @tailrec def reverseAcc(acc: List[A], list: List[A]): List[A] = list match {
      case Nil ⇒ acc
      case x :: xs ⇒ reverseAcc(x :: acc, xs)
    }

    reverseAcc(Nil, list)
  }
}