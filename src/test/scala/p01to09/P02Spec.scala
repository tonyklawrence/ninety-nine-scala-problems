package p01to09

import org.specs2.mutable.Specification
import scala.annotation.tailrec

class P02Spec extends Specification {
  import P02.penultimate

  "Find the last but one element of a list" >> {
    penultimate(List(1, 1, 2, 3, 5, 8)) must_== 5
  }

  "Cannot get penultimate of empty list" >> {
    penultimate(List.empty[Int]) must throwA(new NoSuchElementException)
  }

  "Cannot get penultimate of list with single item" >> {
    penultimate(List(1)) must throwA(new NoSuchElementException)
  }
}

object P02 {
  @tailrec def penultimate[A](list: List[A]): A = list match {
    case Nil ⇒ throw new NoSuchElementException
    case x :: Nil ⇒ throw new NoSuchElementException
    case x :: y :: Nil ⇒ x
    case x :: xs ⇒ penultimate(xs)
  }
}