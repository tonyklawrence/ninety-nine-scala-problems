package p01to09

import org.specs2.mutable.Specification
import scala.annotation.tailrec

class P01Spec extends Specification {
  import P01.last

  "Find the last element of a list" >> {
    last(List(1, 1, 2, 3, 5, 8)) must_== 8
  }

  "Cannot get last of empty list" >> {
    last(List.empty[Int]) must throwA(new NoSuchElementException)
  }
}

object P01 {
  @tailrec def last[A](list: List[A]): A = list match {
    case Nil ⇒ throw new NoSuchElementException
    case x :: Nil ⇒ x
    case x :: xs ⇒ last(xs)
  }
}