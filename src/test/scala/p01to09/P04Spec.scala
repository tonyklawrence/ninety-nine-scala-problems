package p01to09

import org.specs2.mutable.Specification
import scala.annotation.tailrec

class P04Spec extends Specification {

  "Find the number of elements of a list" >> {
    P04.length(List(1, 1, 2, 3, 5, 8)) must_== 6
  }

  "Can get length of empty list" >> {
    P04.length(List.empty[Int]) must_== 0
  }
}

object P04 {
  def length[A](list: List[A]): Int = {
    @tailrec def lengthAcc(acc: Int, list: List[A]): Int = list match {
      case Nil ⇒ acc
      case x :: Nil ⇒ acc + 1
      case x :: xs ⇒ lengthAcc(acc + 1, xs)
    }

    lengthAcc(0, list)
  }
}