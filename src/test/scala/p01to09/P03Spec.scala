package p01to09

import org.specs2.mutable.Specification
import scala.annotation.tailrec

class P03Spec extends Specification {
  import P03.nth

  "Find the Kth element of a list" >> {
    nth(2, List(1, 1, 2, 3, 5, 8)) must_== 2
  }

  "Can't find element greater than size of list" >> {
    nth(100, List(1)) must throwA(new NoSuchElementException)
  }

  "Can't find element in empty list" >> {
    nth(0, List.empty[Int]) must throwA(new NoSuchElementException)
  }

  "Negative index does not make sense" >> {
    nth(-1, List(1, 2, 3)) must throwA(new NoSuchElementException)
  }
}

object P03 {
  @tailrec def nth[A](index: Int, list: List[A]): A = list match {
    case Nil ⇒ throw new NoSuchElementException
    case x :: Nil ⇒ if (index == 0) x else throw new NoSuchElementException
    case x :: xs ⇒ if (index == 0) x else nth(index - 1, xs)
  }
}
