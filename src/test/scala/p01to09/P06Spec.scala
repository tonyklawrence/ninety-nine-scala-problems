package p01to09

import org.specs2.mutable.Specification

class P06Spec extends Specification {
  import P06.isPalindrome

  "Find out whether a list is a palindrome" >> {
    isPalindrome(List(1, 2, 3, 2, 1)) must beTrue
  }

  "This one isnt" >> {
    isPalindrome(List(1, 2, 3, 4, 5)) must beFalse
  }
}

import P05.reverse

object P06 {
  def isPalindrome[A](list: List[A]): Boolean = reverse(list) == list
}